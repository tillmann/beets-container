# Beets Container - a container containing beets

Builds a minimal image containing [beets](https://github.com/beetbox/beets/).
Beets is built from the last commit on the master branch.

## Usage

Define a handy alias to use beets comfortably.

```bash
alias 'beet=podman run --rm -v ./beets.yml:/beets.yml:ro,Z -v /path/to/music/library:/audio:z $PODMAN_OPTS registry.gitlab.com/tillmann/beets-container:latest -c /beets.yml'

beet --help
```

Supply additional options to podman on running the alias, e.g. to import from additional volumes.

```bash
PODMAN_OPTS="--security-opt label=disable -v /var/mounted/library/to/import:/import:ro" && beet import /import
```
