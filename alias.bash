alias 'beet=podman run \
         --pull=newer \
         --rm -it \
         -v ./beets.yml:/beets.yml:ro,Z \
         -v /path/to/music/library:/audio:z \
         $PODMAN_OPTS \
         registry.gitlab.com/tillmann/beets-container:latest -c /beets.yml'
