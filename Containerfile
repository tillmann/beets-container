FROM python:3.12-alpine as builder

# jellyfish needs cargo
RUN apk add cargo

# install from working git tarball
RUN pip install --user --no-cache-dir https://github.com/beetbox/beets/tarball/810af1f

# the lastgenre plugin needs pylast
RUN pip install --user pylast beetcamp

# multi-stage build: we don't need cargo to run beet
FROM python:3.12-alpine

# but we need libgcc to run what cargo produced
RUN apk add libgcc

# copy what we produced in the first stage
COPY --from=builder /root/.local /root/.local

# set PATH so we find beet
ENV PATH=/root/.local/bin:$PATH

# beet is the entrypoint
ENTRYPOINT ["beet"]
