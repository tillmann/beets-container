#!/bin/sh
# fail on error and say what we run
set -e -x

# get current commit-id of beets main branch
COMMIT_ID=$(git ls-remote -h --refs https://github.com/beetbox/beets.git master | cut -c1-7)
SLUG=beets-${COMMIT_ID}

# checkout main branch
git checkout main

# update local containerfile with new commit-id
sed -i "s_tarball/.*_tarball/${COMMIT_ID}_" Containerfile

# add and commit changes in Containerfile, exit gracefully if there were no changes
git commit -am "scheduled: bump beets commit-id to ${COMMIT_ID}" || exit 0

# build image(s) from new Containerfile
podman build -t ${CI_REGISTRY_IMAGE-localhost}:$SLUG -f Containerfile

# run small test
podman run --rm ${CI_REGISTRY_IMAGE-localhost}:$SLUG --version

# and push
# CI_BUMP_BEETS_JOB_TOKEN is a Project Access Token with write access. It needs
# to be properly set in two locations:
#
#  - https://gitlab.com/tillmann/beets-container/-/settings/access_tokens
#  - https://gitlab.com/tillmann/beets-container/-/settings/ci_cd#Variables
#
git push https://bump-beets-job:${CI_BUMP_BEETS_JOB_TOKEN}@gitlab.com/${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME}.git
